import unittest
from unittest.mock import patch, MagicMock
import json
from operatorsWrite import fetch_rating_data, fetch_profile_data
from operatorsRead import read_profile_data_and_insert_to_mongodb, read_rating_data_and_insert_to_mongodb

class TestOperators(unittest.TestCase):

    @patch('requests.get')
    def test_fetch_profile_data(self, mock_get):
        mock_response = MagicMock()
        mock_response.json.return_value = {"profile": "test_profile"}
        mock_get.return_value = mock_response

        fetch_profile_data()

        mock_get.assert_called_once()
        with open('/tmp/profile_data.json', 'r') as json_file:
            data = json.load(json_file)
        self.assertEqual(data["profile"], {"profile": "test_profile"})

    @patch('requests.get')
    def test_fetch_rating_data(self, mock_get):
        mock_response = MagicMock()
        mock_response.json.return_value = {"rating": "test_rating"}
        mock_get.return_value = mock_response

        fetch_rating_data()

        mock_get.assert_called_once()
        with open('/tmp/rating_data.json', 'r') as json_file:
            data = json.load(json_file)
        self.assertEqual(data["rating"], {"rating": "test_rating"})

    @patch('pymongo.MongoClient')
    def test_read_profile_data_and_insert_to_mongodb(self, mock_mongo_client):
        mock_db = MagicMock()
        mock_mongo_client.return_value.__getitem__.return_value = mock_db

        read_profile_data_and_insert_to_mongodb()

        mock_mongo_client.assert_called_once_with("mongodb://mongodb:27017/")
        mock_db.__getitem__.assert_called_once_with("profile_collection")
        mock_db.__getitem__.return_value.insert_one.assert_called_once()

    @patch('pymongo.MongoClient')
    def test_read_rating_data_and_insert_to_mongodb(self, mock_mongo_client):
        mock_db = MagicMock()
        mock_mongo_client.return_value.__getitem__.return_value = mock_db

        read_rating_data_and_insert_to_mongodb()

        mock_mongo_client.assert_called_once_with("mongodb://mongodb:27017/")
        mock_db.__getitem__.assert_called_once_with("rating_collection")
        mock_db.__getitem__.return_value.insert_one.assert_called_once()

if __name__ == '__main__':
    unittest.main()