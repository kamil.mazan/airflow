
import datetime
from datetime import timedelta
import requests
import pymongo
import json


# Definition des url afin de recupérer le profile et le classement
urlProfile = "https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=b7scpSOwHqxtjwh97Zn5EJtqDVKvxqoz"
urlRating = "https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=b7scpSOwHqxtjwh97Zn5EJtqDVKvxqoz"



def fetch_profile_data():
    # Extraction des information demandé grâce à l'api, en json
    profile_response = requests.get(urlProfile).json()
    data = {
        "profile": profile_response,
        "timestamp": datetime.datetime.now().isoformat()
    }
    with open('/tmp/profile_data.json', 'w') as json_file:
        json.dump(data, json_file)


def fetch_rating_data():
    # Extraction des information demandé grâce à l'api, en json
    rating_response = requests.get(urlRating).json()
    data = {
        "rating": rating_response,
        "timestamp": datetime.datetime.now().isoformat()
    }
    with open('/tmp/rating_data.json', 'w') as json_file:
        json.dump(data, json_file)

