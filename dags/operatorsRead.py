
import datetime
from datetime import timedelta
import requests
import pymongo
import json


# Definition des url afin de recupérer le profile et le classement
urlProfile = "https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=b7scpSOwHqxtjwh97Zn5EJtqDVKvxqoz"
urlRating = "https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=b7scpSOwHqxtjwh97Zn5EJtqDVKvxqoz"



def read_profile_data_and_insert_to_mongodb():
    # Read profile data from the JSON file
    with open('/tmp/profile_data.json', 'r') as json_file:
        profile_data = json.load(json_file)
    client = pymongo.MongoClient("mongodb://mongodb:27017/")  
    db = client["mydatabase"]
    collection = db["profile_collection"]
    inserted_data = collection.insert_one(profile_data)


def read_rating_data_and_insert_to_mongodb():
    # Read rating data from the JSON file
    with open('/tmp/rating_data.json', 'r') as json_file:
        rating_data = json.load(json_file)
    client = pymongo.MongoClient("mongodb://mongodb:27017/") 
    db = client["mydatabase"]
    collection = db["rating_collection"]
    inserted_data = collection.insert_one(rating_data)