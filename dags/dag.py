from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from operatorsWrite import fetch_rating_data, fetch_profile_data
from operatorsRead import read_profile_data_and_insert_to_mongodb, read_rating_data_and_insert_to_mongodb

# Define the default arguments for the DAG
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2024, 3, 6),
    'retry_delay': timedelta(minutes=1),
}

# Define the DAG
dag = DAG(
    'fetch_data_and_insert_to_mongodb',
    default_args=default_args,
    description='A DAG to fetch data from API and insert into MongoDB every minute',
    schedule_interval=timedelta(minutes=1),  # Run the DAG every minute
    catchup=False,
)

# Define the task to fetch and store profile data in a JSON file
fetch_profile_task = PythonOperator(
    task_id='fetch_profile_data_task',
    python_callable=fetch_profile_data,
    dag=dag,
)

# Define the task to fetch and store rating data in a JSON file
fetch_rating_task = PythonOperator(
    task_id='fetch_rating_data_task',
    python_callable=fetch_rating_data,
    dag=dag,
)

# Define the task to read profile data from JSON file and insert into MongoDB
insert_profile_to_mongodb_task = PythonOperator(
    task_id='insert_profile_to_mongodb_task',
    python_callable=read_profile_data_and_insert_to_mongodb,
    dag=dag,
)

# Define the task to read rating data from JSON file and insert into MongoDB
insert_rating_to_mongodb_task = PythonOperator(
    task_id='insert_rating_to_mongodb_task',
    python_callable=read_rating_data_and_insert_to_mongodb,
    dag=dag,
)

# Set task dependencies
fetch_profile_task >> insert_profile_to_mongodb_task
fetch_rating_task >> insert_rating_to_mongodb_task
